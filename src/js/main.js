function addActiveClass() {
    const burgerBtn = document.getElementById("burger");
    const menu = document.getElementById("menu");
    if (burgerBtn) {
      burgerBtn.addEventListener('click', function() {
          menu.classList.toggle('open');
      });
    }
  };

  addActiveClass();